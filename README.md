# TFetch: A P2P File Sharing Network

## Introduction
TFetch is a work in progress aimed at providing a basis for academic analysis of various [Distributed Hash Table](https://en.wikipedia.org/wiki/Distributed_hash_table) Implementations.

The first goal is to implement [Tapestry DHT algorithm](https://ieeexplore.ieee.org/abstract/document/1258114), proposed by Zaho *et. al.* for the routing purposes. This is in part inspired by the [Jami package](https://jami.net/), based on [Kademlia DHT algorithm](https://pdos.csail.mit.edu/~petar/papers/maymounkov-kademlia-lncs.pdf), developed by Maymounkov *et. al.* along with a fast polling based TCP/IP server for fetching the actual files.
The files are planned to be stored in a sharded and replicated manner to provide availability, reliability and efficiency.

This is a hobby project, but with serious resolve,  which is work in progress and all contributions are welcome.

## Depends Upon
* [GNU Make](http://www.gnu.org/software/make/)
* [Clogger](https://gitlab.com/r0ck3r008/clogger)

## Operating System Compatibility
* Built and tested on GNU/Linux ([Arch Linux](archlinux.org)).

## Compilation Process
``` bash
# Compile
make
# Clean
make clean
# Deep CLean
make distclean
```
