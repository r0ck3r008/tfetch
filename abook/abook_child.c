#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<pthread.h>

#include"clogger/clogger.h"
#include"utils/defs.h"
#include"utils/sock_handler.h"
#include"abook/abook_child.h"

extern Logger *logger;

AbookChild *
abook_child_init(char *addr, int sock)
{
	AbookChild *achild=calloc(1, sizeof(AbookChild));
	if(achild==NULL) {
		logger_msg(logger, LOG_ERR,
			"ABOOKCHILD: Malloc: Error in allocating memory!");
			_exit(-1);
	}
	achild->psock=sock;
	if((achild->lsock=sock_create(addr, SOCK_DGRAM, 1))<0)
		_exit(-1);
	achild->ptrmap=ptrmap_init();
	achild->pq=pque_init(pque_greater);

	return achild;
}

void
abook_child_loop(AbookChild *achild)
{
	int lsock=achild->lsock;
	while(1) {
		char buf[BUF_SZ]={0};
		struct sockaddr_in addr;
		if(!sock_udprd(buf, lsock, &addr))
			_exit(-1);
	}
}

void
abook_child_deinit(AbookChild *achild)
{
	close(achild->psock);
	close(achild->lsock);
	pque_deinit(achild->pq);
	ptrmap_deinit(achild->ptrmap);
	free(achild);
}
