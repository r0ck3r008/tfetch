#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include"clogger/clogger.h"
#include"utils/alloc.h"
#include"utils/hash.h"
#include"abook/nbrtbl.h"

extern Logger *logger;

NbrTbl *
nbrtbl_init(char *hash)
{
	NbrTbl *ntbl=calloc(1, sizeof(NbrTbl));
	if(ntbl==NULL) {
		logger_msg(logger, LOG_ERR,
				"NBRTBL: Malloc: Error in allocating memory!");
		_exit(-1);
	}
	ntbl->hash=alloc_charcpy(hash);

	return ntbl;
}

void
nbrtbl_add(NbrTbl *ntbl, char *hash, int tcp_sock, struct sockaddr_in *addr)
{
	int lvl=hash_getlvl(ntbl->hash, hash, MAXDEPTH);
	Ptree *ptree=ntbl->map[lvl];
	if(ptree!=NULL && ptree->nmemb==MAXDEPTH)
		return;
	if(ptree==NULL)
		ptree=ptree_init();

	Nbr *nbr=calloc(1, sizeof(Nbr));
	if(nbr==NULL){
		logger_msg(logger, LOG_ERR,
				"NBR: Malloc: Error in allocating memory!");
	}
	nbr->tcp_sock=tcp_sock;
	nbr->addr=addr;

	if(ntbl->nlist==NULL)
		ntbl->nlist=nbr;
	else
		ntbl->nlist_end->next=nbr;
	ntbl->nlist_end=nbr;
	ptree_add(ptree, hash, (void *)nbr);
}

Nbr *
nbrtbl_find(NbrTbl *ntbl, char *hash)
{
	int lvl=hash_getlvl(ntbl->hash, hash, MAXDEPTH);
	if(ntbl->map[lvl]==NULL)
		return NULL;

	return (Nbr *)(ptree_search(ntbl->map[lvl], hash));
}

void
nbrtbl_deinit(NbrTbl *ntbl)
{
	if(ntbl==NULL)
		return;
	free(ntbl->hash);
	Nbr *curr=ntbl->nlist->next;
	while(curr!=NULL) {
		ntbl->nlist->next=curr->next;
		free(curr);
		curr=ntbl->nlist->next;
	}
	free(ntbl->nlist);
	for(int i=0; i<MAXDEPTH; i++)
		if(ntbl->map[i]!=NULL)
			ptree_deinit(ntbl->map[i]);
	free(ntbl);
}
