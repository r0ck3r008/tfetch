#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>

#include"clogger/clogger.h"
#include"abook/abook_child.h"
#include"utils/sock_handler.h"
#include"utils/alloc.h"
#include"abook/abook.h"

extern Logger *logger;

Abook *
abook_init(char *addr)
{
	Abook *abook=calloc(1, sizeof(Abook));
	if(abook==NULL) {
		logger_msg(logger, LOG_ERR,
				"ABOOK: Malloc: Error in allocating memory!");
		_exit(-1);
	}
	int sockpair[2]={0};
	sock_sockpair(sockpair);

	pid_t child=fork();
	if(!child) {
		/* child */
		AbookChild *achild=abook_child_init(addr, sockpair[0]);
		abook_child_deinit(achild);
		_exit(1);
	} else if(child==-1) {
		/* error */
		logger_msg(logger, LOG_ERR,
				"ABOOK: Fork: %s", strerror(errno));
		_exit(-1);
	} else {
		/* parent */
		abook->sock=sockpair[1];
		abook->abook_pid=child;
		logger_msg(logger, LOG_DBG,
				"ABOOK: Child forked at pid: %d", child);
	}

	return abook;
}

void
abook_deinit(Abook *abook)
{
	if(!sock_tcpwr("EXIT", abook->sock))
		logger_msg(logger, LOG_WRN,
			"ABOOK: Exit: Error in shutting down the child!");
	close(abook->sock);

	if(waitpid(abook->abook_pid, NULL, 0)<0) {
		logger_msg(logger, LOG_ERR, "ABOOK: Wait: %s!");
		_exit(-1);
	}
	free(abook);
}
