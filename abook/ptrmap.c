#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include"utils/alloc.h"
#include"clogger/clogger.h"
#include"abook/ptrmap.h"

extern Logger *logger;

PtrMap *
ptrmap_init()
{
	PtrMap *ptrmap=calloc(1, sizeof(PtrMap));
	if(ptrmap==NULL) {
		logger_msg(logger, LOG_ERR,
				"PTRMAP: Malloc: Error in allocating memory!");
		_exit(-1);
	}
	ptrmap->ptree=ptree_init();

	return ptrmap;
}

void
ptrmap_add(PtrMap *ptrmap, char *hash)
{
	ptree_add(ptrmap->ptree, hash, NULL);
}

int
ptrmap_exists(PtrMap *ptrmap, char *hash)
{
	return ptree_exists(ptrmap->ptree, hash);
}

void
ptrmap_deinit(PtrMap *ptrmap)
{
	ptree_deinit(ptrmap->ptree);
	free(ptrmap);
}
