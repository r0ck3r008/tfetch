COMPILER := gcc
COMPILER_FLAGS := '-I${shell pwd}/include -g'
LINKER_FLAGS := -L${shell pwd}/include/clogger -lclogger

ALL_OBJS := utils/*.o abook/*.o

all: utils_objs abook_objs istore_objs clogger_lib
	${COMPILER} ${ALL_OBJS} ${LINKER_FLAGS} -o bin/tfetch.out
	make clean_objs

utils_objs:
	COMPILER=${COMPILER} COMPILER_FLAGS=${COMPILER_FLAGS} \
			make -C utils/

abook_objs:
	COMPILER=${COMPILER} COMPILER_FLAGS=${COMPILER_FLAGS} \
			make -C abook/

istore_objs:
	COMPILER=${COMPILER} COMPILER_FLAGS=${COMPILER_FLAGS} \
			make -C istore/

clogger_lib:
	make -C include/clogger

clean_objs:
	make -C utils/ clean
	make -C abook/ clean
	make -C istore/ clean
	make -C include/clogger/ clean

clean: clean_objs
	rm -f bin/tfetch.out
