#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<ctype.h>

#include"clogger/clogger.h"
#include"utils/alloc.h"
#include"utils/ptree.h"

extern Logger *logger;

char *ptree_charcpy(char *_str)
{
	if(_str==NULL)
		return NULL;
	char *str=strdup(_str);
	if(str==NULL) {
		logger_msg(logger, LOG_ERR,
				"PTREE: MALLOC: Error in allocating memory!");
		_exit(-1);
	}

	return str;
}

int
ptree_hashit(char c)
{
	int ret=0;
	if(isdigit(c))
		ret=(c-'0');
	else if(isalpha(c))
		ret=(c-'a')+10;

	return ret;
}

int
ptree_getlvl(char *a, int alen, char *b, int blen)
{
	if(a==NULL)
		return 0;
	int lvl=0, minlen=((alen<=blen) ? (alen) : (blen));
	while(lvl<minlen && a[lvl]==b[lvl])
		lvl++;

	return lvl;
}

Pnode *
pnode_init(char *c, int len)
{
	Pnode *pnode=calloc(1, sizeof(Pnode));
	if(pnode==NULL) {
		logger_msg(logger, LOG_ERR,
				"PNODE: MALLOC: Error in allocating memory!");
		_exit(-1);
	}
	pnode->str=ptree_charcpy(c);
	pnode->len=len;

	return pnode;
}

Ptree *
ptree_init()
{
	Ptree *ptree=calloc(1, sizeof(Ptree));
	if(ptree==NULL) {
		logger_msg(logger, LOG_ERR,
				"PTREE: MALLOC: Error in allocating memory!");
		_exit(-1);
	}

	return ptree;
}

void
pnode_add(Pnode *pnode, char *str, int slen, void *pload)
{
	int lvl=ptree_getlvl(pnode->str, pnode->len, str, slen),
		indx=ptree_hashit(str[lvl]);
	if(pnode->child[indx]==NULL)
		pnode->child[indx]=pnode_init(&str[lvl], slen-lvl);
	else
		pnode_add(pnode->child[indx], &str[lvl], slen-lvl, pload);

	if(lvl) {
		indx=ptree_hashit(pnode->str[lvl]);
		char *oldstr=pnode->str, newstr[lvl+1];
		strncpy(newstr, oldstr, sizeof(char)*lvl);
		newstr[lvl]='\0';
		pnode->str=ptree_charcpy(newstr);
		if(pnode->child[indx]==NULL)
			pnode->child[indx]=pnode_init(&oldstr[lvl],
							pnode->len-lvl);
		else
			pnode_add(pnode->child[indx], &oldstr[lvl],
							pnode->len-lvl, pload);
		pnode->len=lvl+1;

		free(oldstr);
	}
}

void
ptree_add(Ptree *ptree, char *str, void *pload)
{
	if(ptree->root==NULL)
		ptree->root=pnode_init(NULL, 0);
	ptree->nmemb++;

	pnode_add(ptree->root, str, strlen(str), pload);
}

void *
pnode_search(Pnode *node, char *term, int tlen)
{
	if(node==NULL) {
		return NULL;
	} else if(tlen==0) {
		return node->pload;
	} else if(term[0]=='.') {
		void *ret=NULL;
		for(int i=0; i<36; i++) {
			if(node->child[i]!=NULL &&
					(ret=pnode_search(node->child[i],
						&term[1], tlen-1))!=NULL) {
				break;
			}
		}
		return ret;
	} else if(node->str!=NULL && !strcmp(term, node->str)) {
		return node->pload;
	}

	int lvl=ptree_getlvl(node->str, node->len, term, tlen),
		indx=ptree_hashit(term[lvl]);
	return pnode_search(node->child[indx], &term[lvl], tlen-lvl);
}

void *
ptree_search(Ptree *ptree, char *term)
{
	return pnode_search(ptree->root, term, strlen(term));
}

int
ptree_exists(Ptree *ptree, char *hash)
{
	if(ptree_search(ptree, hash)!=NULL)
		return 1;
	return 0;
}

void
pnode_deinit(Pnode *pnode)
{
	if(pnode==NULL)
		return;

	free(pnode->str);
	for(int i=0; i<36; i++)
		pnode_deinit(pnode->child[i]);
	free(pnode);
}

void
ptree_deinit(Ptree *ptree)
{
	pnode_deinit(ptree->root);
	free(ptree);
}
