#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#include"clogger/clogger.h"
#include"utils/pque.h"

extern Logger *logger;

inline int
pque_greater(int *pq, int indx1, int indx2)
{
	return (pq[indx1] > pq[indx2]);
}

inline int
pque_lesser(int *pq, int indx1, int indx2)
{
	return (pq[indx1] < pq[indx2]);
}

void
pque_realloc(Pque *pq)
{
	int *new=calloc(pq->size*2, sizeof(int));
	if(pq==NULL) {
		logger_msg(logger, LOG_ERR,
			"PQUE: Malloc: Error in allocating memory!\n");
		_exit(-1);
	}

	for(int i=0; i<pq->nmemb; i++)
		new[i]=pq->pq[i];

	free(pq->pq);
	pq->pq=new;
}

void
pque_swap(int *a, int *b)
{
	int tmp=*a;
	*a=*b;
	*b=tmp;
}

int
pque_getpindx(int indx)
{
	if(indx%2)
		/* Left Child */
		return ((indx-1)/2);
	else
		/* Right Child */
		return ((indx-2)/2);
}

int
pque_bbldwn(Pque *pq, int cpindx)
{
	int child=2*cpindx+1;
	while(child<pq->nmemb) {
		if(!pq->comp(pq->pq, child, cpindx) &&
			((++child)>=(pq->nmemb) ||
			!pq->comp(pq->pq, child, cpindx)))
			break;
		pque_swap(&(pq->pq[cpindx]), &(pq->pq[child]));
		cpindx=child;
		child=2*cpindx+1;
	}
}

int
pque_bblup(Pque *pq, int cindx)
{
	int cpindx=pq->cpindx, curr=cindx;
	while(cpindx>=0 && !pq->comp(pq->pq, cpindx, curr)) {
		pque_swap(&(pq->pq[cpindx]), &(pq->pq[curr]));
		curr=cpindx;
		cpindx=pque_getpindx(curr);
	}

	if(cindx!=curr)
		return 1;
	return 0;
}

Pque *
pque_init(int (*comp) (int *, int, int))
{
	Pque *pq=calloc(1, sizeof(Pque));
	if(pq==NULL) {
		logger_msg(logger, LOG_ERR,
			"PQUE: Malloc: Error in allocating memory!\n");
		_exit(-1);
	}
	pq->pq=calloc(16, sizeof(int));
	if(pq==NULL) {
		logger_msg(logger, LOG_ERR,
			"PQUE: Malloc: Error in allocating memory!\n");
		_exit(-1);
	}

	pq->size=16;
	pq->comp=comp;

	return pq;
}

void
pque_push(Pque *pq, int val)
{
	if(pq->nmemb==pq->size)
		pque_realloc(pq);
	int indx=pq->nmemb;
	pq->pq[indx]=val;
	pq->nmemb++;

	if(indx)
		pque_bblup(pq, indx);

	if(indx && indx%2==0)
		/* If its right child, move inc cpindx */
		pq->cpindx++;
}

int
pque_top(Pque *pq)
{
	return pq->pq[0];
}

int
pque_remove(Pque *pq, int val)
{
	int cindx=0, nmemb=pq->nmemb;
	if(val!=-1) {
		int flag=0;
		for(int i=0; i<nmemb; i++) {
			if(pq->pq[i]==val) {
				cindx=i;
				flag=1;
				break;
			}
		}
		if(!flag)
			return -1;
	}

	int ret=pq->pq[cindx];
	pq->nmemb--;
	if(cindx==nmemb-1)
		goto exit;
	else
		pq->pq[cindx]=pq->pq[nmemb-1];

	if(!cindx || !pque_bblup(pq, cindx))
		pque_bbldwn(pq, cindx);

exit:
	if(nmemb%2)
		pq->cpindx--;
	pq->pq[nmemb-1]=0;
	return ret;
}

void
pque_deinit(Pque *pq)
{
	free(pq->pq);
	free(pq);
}
