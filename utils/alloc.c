#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#include"clogger/clogger.h"
#include"utils/alloc.h"

extern Logger *logger;

char *
alloc_charcpy(char *_str)
{
	if(_str==NULL)
		return NULL;
	char *str=strdup(_str);
	if(str==NULL) {
		logger_msg(logger, LOG_ERR,
			"ALLOC: Strdup: Error in allocating memory!");
		_exit(-1);
	}

	return str;
}

char *
alloc_charalloc(int size)
{
	char *str=calloc(size, sizeof(char));
	if(str==NULL) {
		logger_msg(logger, LOG_ERR,
			"ALLOC: Calloc: Error in allocating memory!");
		_exit(-1);
	}

	return str;
}
