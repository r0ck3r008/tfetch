#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdarg.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<errno.h>

#include"clogger/clogger.h"
#include"utils/defs.h"
#include"utils/alloc.h"
#include"utils/sock_handler.h"

extern Logger *logger;

int
sock_create(char *_addr, int sock_type, int srv)
{
	int sock;
	if((sock=socket(AF_INET, sock_type, 0))<0)  {
		logger_msg(logger, LOG_ERR,
			"SOCK: Socket: %s: %s", _addr, strerror(errno));
		return -1;
	}

	struct sockaddr_in sock_addr;
	explicit_bzero(&sock_addr, sizeof(struct sockaddr_in));
	sock_addr.sin_family=AF_INET;
	char *addr=alloc_charcpy(_addr);
	sock_addr.sin_addr.s_addr=inet_addr(strtok(addr, ":"));
	sock_addr.sin_port=htons((int)strtol(strtok(NULL, ":"), NULL, 10));

	int error=0;
	if(srv) {
		if(bind(sock, (struct sockaddr *)&sock_addr,
			sizeof(struct sockaddr_in))<0) {
			logger_msg(logger, LOG_ERR,
				"SOCK: Bind: %s: %s", _addr, strerror(errno));
			error=1; goto exit;
		}

		if(listen(sock, 5)<0) {
			logger_msg(logger, LOG_ERR,
				"SOCK: Listen: %s: %s", _addr, strerror(errno));
			error=1; goto exit;
		}

		logger_msg(logger, LOG_DBG,
				"SOCK: %s bound and listening", _addr);
	} else {
		if(connect(sock, (struct sockaddr *)&sock_addr,
			sizeof(struct sockaddr))<0) {
			logger_msg(logger, LOG_ERR,
				"SOCK: Connect: %s: %s", _addr, strerror(errno));
			error=1; goto exit;
		}

		logger_msg(logger, LOG_DBG,
				"SOCK: %s connected!", _addr);
	}

exit:
	if(error) {
		close(sock);
		return -1;
	}

	return sock;
}

void
sock_sockpair(int *sockpair)
{
	if(socketpair(AF_UNIX, SOCK_STREAM, 0, sockpair)<0) {
		logger_msg(logger, LOG_ERR,
				"SOCK: Socketpair: %s", strerror(errno));
		_exit(-1);
	}
}

int
sock_udprd(char *buf, int sock, struct sockaddr_in *addr)
{
	socklen_t addrlen=sizeof(struct sockaddr_in);
	if(recvfrom(sock, buf, sizeof(char)*BUF_SZ, 0,
			(struct sockaddr *)addr, &addrlen)<0) {
		logger_msg(logger, LOG_ERR,
				"SOCK: UDP Recv: %s", strerror(errno));
		return 0;
	}

	return 1;
}

int
sock_tcprd(char *buf, int sock)
{
	if(recv(sock, buf, sizeof(char)*BUF_SZ, 0)<0) {
		logger_msg(logger, LOG_ERR,
				"SOCK: TCP Recv: %s", strerror(errno));
		return 0;
	}

	return 1;
}

int
sock_udpwr(char *buf, int sock, struct sockaddr_in *addr)
{
	int ret=1;
	socklen_t addrlen=sizeof(struct sockaddr_in);
	if(sendto(sock, buf, sizeof(char)*BUF_SZ, 0,
			(struct sockaddr *)addr, addrlen)<0)
	{
		logger_msg(logger, LOG_ERR,
				"SOCK: UDP Send: %s", strerror(errno));
		ret=0;
	}

	free(buf);
	return ret;
}

int
sock_tcpwr(char *buf, int sock)
{
	int ret=1;
	if(send(sock, buf, sizeof(char)*BUF_SZ, 0)<0) {
		logger_msg(logger, LOG_ERR,
				"SOCK: TCP Send: %s", strerror(errno));
		ret=0;
	}

	return ret;
}

int
sock_max(int n, ...)
{
	va_list args;
	va_start(args, n);
	int max_sock=-1;
	for(int i=0; i<n; i++) {
		int sock=va_arg(args, int);
		if(sock>max_sock)
			max_sock=sock;
	}

	va_end(args);
	return max_sock;
}
