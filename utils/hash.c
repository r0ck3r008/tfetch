#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<openssl/sha.h>
#include<unistd.h>

#include"utils/alloc.h"
#include"clogger/clogger.h"
#include"utils/hash.h"

extern Logger *logger;

char *
hash_sha512(unsigned char *str, int len, int trunc)
{
	if(trunc%2) {
		logger_msg(logger, LOG_ERR,
				"HASH: Truncate length should be even!");
		return NULL;
	}
	unsigned char *hash_buf=calloc(SHA512_DIGEST_LENGTH,
					sizeof(unsigned char));
	if(hash_buf==NULL) {
		logger_msg(logger, LOG_ERR,
				"HASH: Malloc: Error in allocating memory!");
		return NULL;
	}

	if(SHA512(str, sizeof(unsigned char)*len, hash_buf)!=hash_buf) {
		logger_msg(logger, LOG_ERR,
				"HASH: SHA512: Error in hashing!");
		return NULL;
	}

	char *hash_str=alloc_charalloc(trunc);
	for(int i=0; i<(trunc/2); i++)
		sprintf(&(hash_str[i*2]), "%02x", (unsigned char)hash_buf[i]);

	free(hash_buf);
	return hash_str;
}

int
hash_getlvl(char *hash1, char *hash2, int max_lvl)
{
	int lvl=0;
	for(int i=0; i<max_lvl; i++) {
		if(hash1[i]!=hash2[i])
			break;
		lvl++;
	}

	return lvl;
}
