#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>

#include"utils/sock_handler.h"
#include"istore/istore.h"
#include"istore/istore_child.h"
#include"clogger/clogger.h"

extern Logger *logger;

Istore *
istore_init(char *addr)
{
	Istore *istore=calloc(1, sizeof(Istore));
	if(istore==NULL) {
		logger_msg(logger, LOG_ERR,
				"ISTORE: Malloc: Error in allocating memory!");
		_exit(-1);
	}

	int sock[2]={0};
	sock_sockpair(sock);

	pid_t child_pid=fork();
	if(child_pid==0) {
		/* child */
		IstoreChild *ichild=istore_child_init(addr, sock[0]);
		istore_child_loop(ichild);
		_exit(1);
	} else if(child_pid==-1) {
		logger_msg(logger, LOG_ERR,
				"ISTORE: Fork: %s", strerror(errno));
		_exit(-1);
	} else {
		/* parent */
		istore->istore_pid=child_pid;
		istore->sock=sock[1];
	}

	return istore;
}

void
istore_deinit(Istore *istore)
{
	if(!sock_tcpwr("EXIT", istore->sock))
		logger_msg(logger, LOG_WRN,
				"ISTORE: Deinit: Unable to send exit msg!");
	close(istore->sock);

	if(waitpid(istore->istore_pid, NULL, 0)<0) {
		logger_msg(logger, LOG_ERR, "ISTORE: Wait: %s!");
		_exit(-1);
	}

	free(istore);
}
