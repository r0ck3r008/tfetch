#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include"utils/alloc.h"
#include"utils/hash.h"
#include"clogger/clogger.h"
#include"istore/objmap.h"

extern Logger *logger;

ObjMap *
objmap_init(char *hash)
{
	ObjMap *omap=calloc(1, sizeof(ObjMap));
	if(omap==NULL) {
		logger_msg(logger, LOG_ERR,
				"OBJMAP: Malloc: Error in allocaing memory!");
		_exit(-1);
	}
	omap->hash=alloc_charcpy(hash);

	return omap;
}

void
objmap_add(ObjMap *omap, char *hash, char *fname)
{
	Obj *obj=calloc(1, sizeof(Obj));
	if(obj==NULL) {
		logger_msg(logger, LOG_ERR,
				"OBJ: Malloc: Error in allocating memory!");
		_exit(-1);
	}
	if(omap->olist==NULL)
		omap->olist=obj;
	else
		omap->olist_end->next=obj;
	omap->olist_end=obj;

	int lvl=hash_getlvl(omap->hash, hash, MAXDEPTH);
	if(omap->map[lvl]==NULL)
		omap->map[lvl]=ptree_init();
	ptree_add(omap->map[lvl], hash, (void *)obj);
}

Obj *
objmap_find(ObjMap *omap, char *hash)
{
	int lvl=hash_getlvl(omap->hash, hash, MAXDEPTH);
	return (Obj *)(ptree_search(omap->map[lvl], hash));
}

void
obj_deinit(Obj *obj)
{
	if(obj==NULL)
		return;

	free(obj->fname);
	free(obj);
}

void
objmap_deinit(ObjMap *omap)
{
	Obj *curr=omap->olist->next;
	while(curr!=NULL) {
		omap->olist->next=curr->next;
		obj_deinit(curr);
		curr=omap->olist->next;
	}
	obj_deinit(omap->olist);

	for(int i=0; i<MAXDEPTH; i++)
		if(omap->map[i]!=NULL)
			ptree_deinit(omap->map[i]);

	free(omap->hash);
	free(omap);
}
