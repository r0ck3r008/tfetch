#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/select.h>
#include<unistd.h>
#include<errno.h>

#include"clogger/clogger.h"
#include"utils/pque.h"
#include"utils/alloc.h"
#include"utils/defs.h"
#include"utils/sock_handler.h"
#include"istore/poll_sloop.h"
#include"istore/poll_floop.h"
#include"istore/istore_child.h"

extern Logger *logger;

int
istore_child_accept(int lsock, fd_set *bkup_set, Pque *pq)
{
	int sock;
	if((sock=accept(lsock, NULL, NULL))<0) {
		logger_msg(logger, LOG_ERR,
				"POLL_LLOP: Accept: %s" , strerror(errno));
		return -1;
	}
	FD_SET(sock, bkup_set);
	pque_push(pq, sock);
	return sock;
}

int
istore_child_passdwn(fd_set *bkup_set, Pque *pq, int sock, int ssock, int fsock)
{
	char buf[BUF_SZ]={0}, cmd[BUF_SZ]={0};
	if(!sock_tcprd(buf, sock))
		return 0;

	char *type=strtok(buf, ":");
	int send_sock;
	sprintf(cmd, "%d:%s", sock, strtok(NULL, ":"));
	if(!strcmp(type, "SET"))
		send_sock=ssock;
	else if(!sprintf(type, "FETCH"))
		send_sock=fsock;
	if(!sock_tcpwr(cmd, send_sock))
		return 0;

	FD_CLR(sock, bkup_set);
	pque_remove(pq, sock);

	return 1;
}

void
istore_child_deinit(IstoreChild *ichild)
{
	for(int i=0; i<2; i++) {
		if(!sock_tcpwr("EXIT", ichild->sock[i][1]))
			logger_msg(logger, LOG_WRN,
			"ISTORECHILD: Deinit: Unable to send exit command!");
		close(ichild->sock[i][1]);
	}

	for(int i=0; i<2; i++) {
		int stat=0;
		if((stat=pthread_join(ichild->tid[i], NULL))!=0) {
			logger_msg(logger, LOG_ERR,
					"ISTORECHILD: Pthread Join: %s!");
			_exit(-1);

		}
	}

	close(ichild->lsock);
	close(ichild->psock);
	pque_deinit(ichild->pq);
	free(ichild);
}

IstoreChild *
istore_child_init(char *addr, int psock)
{
	IstoreChild *istore=calloc(1, sizeof(IstoreChild));
	if(istore==NULL) {
		logger_msg(logger, LOG_ERR,
			"ISTORECHILD: Malloc: Error in allocating memory!");
		_exit(-1);
	}
	istore->psock=psock;
	istore->pq=pque_init(pque_greater);
	if((istore->lsock=sock_create(addr, SOCK_STREAM, 1))<0)
		_exit(-1);

	int stat=0;
	sock_sockpair(istore->sock[0]);
	sock_sockpair(istore->sock[1]);
	if((stat=pthread_create(&(istore->tid[0]), NULL,
				poll_sloop, (void *)&(istore->sock[0][0])))!=0) {
		logger_msg(logger, LOG_ERR,
		"ISTORECHILD: Pthread: Poll sloop: %s!", strerror(errno));
		_exit(-1);
	}
	if((stat=pthread_create(&(istore->tid[1]), NULL,
				poll_floop, (void *)&(istore->sock[1][0])))!=0) {
		logger_msg(logger, LOG_ERR,
		"ISTORECHILD: Pthread: Poll floop: %s!", strerror(errno));
		_exit(-1);
	}

	return istore;
}

void
istore_child_loop(IstoreChild *ichild)
{
	fd_set rdset, bkup_set;
	FD_ZERO(&bkup_set);
	FD_SET(ichild->lsock, &bkup_set);
	FD_SET(ichild->psock, &bkup_set);
	FD_SET(ichild->sock[0][1], &bkup_set);
	FD_SET(ichild->sock[1][1], &bkup_set);

	pque_push(ichild->pq, ichild->lsock);
	pque_push(ichild->pq, ichild->psock);
	pque_push(ichild->pq, ichild->sock[0][1]);
	pque_push(ichild->pq, ichild->sock[1][1]);

	Pque *pq=ichild->pq;
	int lsock=ichild->lsock, ssock=ichild->sock[0][0],
			fsock=ichild->sock[1][0], psock=ichild->psock;

	while(1) {
		rdset=bkup_set;
		int fdmax=pque_top(pq);
		if(select(fdmax, &rdset, NULL, NULL, NULL)<0) {
			logger_msg(logger, LOG_ERR,
				"POLL_LLOOP: Select: %s", strerror(errno));
			break;
		}
		for(int i=0; i<fdmax+1; i++) {
			if(FD_ISSET(i, &rdset)) {
				if(i==lsock &&
				!istore_child_accept(lsock, &bkup_set, pq)) {
					break;
				} else if(i==psock) {
					// send end signal to child and exit
					istore_child_deinit(ichild);
					break;
				} else if(!istore_child_passdwn(&bkup_set, pq,
							i, ssock, fsock)) {
					break;
				}
			}
		}
	}
}
