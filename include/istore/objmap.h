#ifndef OBJMAP_J
#define OBJMAP_H

#include"utils/defs.h"
#include"utils/ptree.h"

typedef struct Obj {
	char *fname;
	struct Obj *next;
} Obj;

typedef struct ObjMap {
	char *hash;
	Ptree *map[MAXDEPTH];
	Obj *olist, *olist_end;
} ObjMap;

ObjMap *objmap_init(char *);
void objmap_add(ObjMap *, char *, char *);
Obj *objmap_find(ObjMap *, char *);
void objmap_deinit(ObjMap *);

#endif
