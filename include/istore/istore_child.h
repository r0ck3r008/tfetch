#ifndef ISTORE_CHILD_H
#define ISTORE_CHILD_H

#include<pthread.h>

#include"utils/pque.h"

typedef struct IstoreChild {
	pthread_t tid[2];
	int sock[2][2], psock, lsock;
	Pque *pq;
} IstoreChild;

IstoreChild *istore_child_init(char *, int);
void istore_child_loop(IstoreChild *);

#endif
