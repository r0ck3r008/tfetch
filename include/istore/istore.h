#ifndef ISTORE_H
#define ISTORE_H

typedef struct Istore {
	pid_t istore_pid;
	int sock;
} Istore;

Istore *istore_init(char *);
void istore_deinit(Istore *);

#endif
