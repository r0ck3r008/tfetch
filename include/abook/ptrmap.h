#ifndef PTRMAP_H
#define PTRMAP_H

#include"utils/ptree.h"

typedef struct PtrMap {
	Ptree *ptree;
} PtrMap;

PtrMap *ptrmap_init();
void ptrmap_add(PtrMap *, char *);
void ptrmap_deinit();

#endif
