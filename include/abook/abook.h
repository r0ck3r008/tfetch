#ifndef ABOOK_H
#define ABOOK_H

typedef struct Abook {
	pid_t abook_pid;
	int sock;
} Abook;

Abook *abook_init(char *);
void abook_deinit(Abook *);

#endif
