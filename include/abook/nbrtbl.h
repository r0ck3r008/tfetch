#ifndef NBRTBL_H
#define NBRTBL_H

#include<netinet/in.h>

#include"utils/defs.h"
#include"utils/ptree.h"

typedef struct Nbr {
	int tcp_sock;
	struct sockaddr_in *addr;
	struct Nbr *next;
} Nbr;

typedef struct NbrTbl {
	char *hash;
	Ptree *map[MAXDEPTH];
	Nbr *nlist, *nlist_end;
} NbrTbl;

NbrTbl *nbrtbl_init(char *);
void nbrtbl_add(NbrTbl *, char *, int,
		struct sockaddr_in *);
Nbr *nbrtbl_find(NbrTbl *, char *);
void nbrtbl_deinit();

#endif
