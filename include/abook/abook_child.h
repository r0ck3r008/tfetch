#ifndef ABOOK_CHILD_H
#define ABOOK_CHILD_H

#include"abook/ptrmap.h"
#include"utils/pque.h"

typedef struct AbookChild {
	int psock, lsock;
	PtrMap *ptrmap;
	Pque *pq;
} AbookChild;

AbookChild *abook_child_init(char *, int);
void abook_child_deinit(AbookChild *);

#endif
