#ifndef PQUE_H
#define PQUE_H

typedef struct Pque {
	int *pq, nmemb, size, cpindx;
	int (*comp) (int *, int, int);
} Pque;

int pque_greater(int *, int, int);
int pque_lesser(int *, int, int);
Pque *pque_init(int (*)(int *, int, int));
void pque_push(Pque *, int);
int pque_top(Pque *);
int pque_remove(Pque *, int);
void pque_deinit(Pque *);

#endif
