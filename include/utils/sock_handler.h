#ifndef SOCK_HANDLER_H
#define SOCK_HANDLER_H

#include"netinet/in.h"

int sock_create(char *, int, int);
void sock_sockpair(int *);
int sock_udprd(char *, int, struct sockaddr_in *);
int sock_tcprd(char *, int);
int sock_udpwr(char *, int, struct sockaddr_in *);
int sock_tcpwr(char *, int);
int sock_max(int, ...);

#endif
