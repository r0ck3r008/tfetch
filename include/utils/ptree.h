#ifndef PTREE_H
#define PTREE_H

typedef struct Pnode {
	char *str;
	int len;
	void *pload;
	struct Pnode *child[36];
} Pnode;

typedef struct Ptree {
	Pnode *root;
	int nmemb;
} Ptree;

Ptree *ptree_init();
void ptree_add(Ptree *, char *, void *);
void *ptree_search(Ptree *, char *);
int ptree_exists(Ptree *, char *);
void ptree_deinit(Ptree *);

#endif
